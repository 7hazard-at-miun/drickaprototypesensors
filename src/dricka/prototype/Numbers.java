package dricka.prototype;

public class Numbers {
    public static float average(float[] arr)
    {
        float result = 0;
        for (float var : arr)
            result += var;

        result = result / arr.length;

        return result;
    }
}
