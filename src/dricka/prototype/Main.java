package dricka.prototype;

import lejos.hardware.Bluetooth;
import lejos.hardware.BrickFinder;
import lejos.hardware.Button;
import lejos.hardware.lcd.LCD;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.remote.nxt.NXTCommConnector;
import lejos.remote.nxt.NXTConnection;
import lejos.remote.nxt.SocketConnection;
import lejos.remote.nxt.SocketConnector;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;
import sun.org.mozilla.javascript.internal.EcmaError;

import java.io.*;
import java.math.RoundingMode;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.DecimalFormat;

public class Main {
    static NXTCommConnector connector = Bluetooth.getNXTCommConnector();
    static volatile boolean stopping = false;
    static float distance = 0;

    public static void socketThread()
    {
        LCD.drawString("Waiting...", 0, 3);
        NXTConnection connection = connector.waitForConnection(0, NXTConnection.RAW);
        LCD.drawString("Connected", 0, 3);

        while(true)
        {
            try {

                InputStream input = connection.openInputStream();
                BufferedReader in = new BufferedReader(new InputStreamReader(input), 1);
                OutputStream output = connection.openOutputStream();
                BufferedWriter out = new BufferedWriter(new OutputStreamWriter(output), 1);

                String distancestr = Float.toString(distance)+"\n";
                //out.write(distancestr.length());
                out.write(distancestr);

                out.close();
                output.close();
            } catch (Exception e)
            {

            }

            if(stopping)
            {
                return;
            }

            Delay.msDelay(100);
        }
    }

    public static void main(String[] args)
    {
        EV3UltrasonicSensor usSensor = new EV3UltrasonicSensor(SensorPort.S1);
        usSensor.enable();

        Thread bt = new Thread() {
            public void run() {
                socketThread();
            }
        };
        bt.start();

        while (true)
        {
            // Get Distance from sensor
            SampleProvider sp = usSensor.getDistanceMode();
            float[] samples = new float[sp.sampleSize()];
            sp.fetchSample(samples, 0);

            // Round to centimeters
            DecimalFormat df = new DecimalFormat("###.#");
            df.setRoundingMode(RoundingMode.DOWN);
            distance = Numbers.average(samples);
            String distancestr = df.format(distance*100);

            // Draw messages
            //LCD.drawString("Info... ", 0, 1);
            LCD.drawString("Distance: "+distancestr, 0, 2);

            // Check for any key presses
            if(Button.waitForAnyPress(100) != 0) break;
        }

        LCD.clear();
        LCD.drawString("Exiting", 0, 1);
        stopping = true;

        // close socket
        try {
            connector.cancel();
        }
        catch (Exception e)
        {
            LCD.drawString("Sockets close failed", 0, 2);
            LCD.drawString(e.getMessage(), 0, 2);
            Delay.msDelay(1000);
        }

        Delay.msDelay(1000);
    }
}
